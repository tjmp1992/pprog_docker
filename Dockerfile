FROM ubuntu:16.04

ARG JAVA_JDK_VERSION=8
ARG JAVA_GRADLE_VERSION=2.3


RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y  software-properties-common && \
    add-apt-repository ppa:webupd8team/java -y && \
    apt-get update && \
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
    apt-get install -y oracle-java8-installer && \
    apt-get clean

RUN apt-get install maven -y

RUN apt-get install git -y

RUN export JAVA_HOME=/usr/lib/jvm/java-8-oracle 

RUN export PATH=$PATH:$JAVA_HOME/bin

RUN git clone https://github.com/spring-guides/gs-spring-boot.git

WORKDIR  /gs-spring-boot/complete

RUN mvn clean install 

EXPOSE 8080

CMD ["java", "-jar", "target/gs-spring-boot-0.1.0.jar"]

